\chapter{OpenStack}
OpenStack è una piattaforma software gratuita ed open-source \footnote{Software non protetto da copyright e liberamente modificabile dagli utenti} per il \textit{cloud computing}, per lo più realizzata come IaaS (\textit{Infrastructure-as-a-service}) per cui i server virtuali ed altre risorse sono resi disponibili ai clienti.\\
La piattaforma è composta da una serie di componenti interconnesse che controllano diverse risorse hardware di elaborazione, memorizzazione e collegamento di rete per mezzo di un centro dati.\\
Gli utenti lo gestiscono attraverso una dashboard basata sulla rete, attraverso strumenti da linea di comando o attraverso dei servizi di rete RESTful (i servizi web \textit{REpresentational State Transfer} (REST), o RESTful, forniscono l'interoperabilità tra i vari sistemi su Internet).\\
Con il termine \textit{cloud computing} intendiamo un modello in cui un provider di servizio di terze parti rende disponibili delle risorse computazionali per il loro consumo in base alle necessità. Un cloud pubblico risulta essere un ambiente \textit{multi-tenant}, questo significa che queste risorse computazionali sono condivise da diversi individui e/o compagnie, con tutti i dati messi in sicurezza con dei metodi di criptazione all'avanguardia.\\

\section{Architettura di sistema} 
Il progetto OpenStack è un piattaforma open-source per il cloud computing che supporta tutti i tipi di ambienti cloud; i principali obbiettivi del progetto sono una semplice implementazione, una forte scalabilità e un ricco insieme di funzionalità. Una grande quantità di esperti in cloud computing contribuiscono a questo progetto.\\
OpenStack offre una soluzione sotto forma di infrastruttura come servizio attraverso l'utilizzo di una varietà di servizi complementari; ogni servizio offre un API (\textit{Application Programmin Interface} \footnote{Insieme di procedure disponibili per i programmatori ai fini di completare un determinato compito all'interno di un certo programma}) che ne facilita l'integrazione.\\
L'esempio di architettura \cite{archsys} del sistema di seguito proposto richiede almeno due nodi (\textit{host}) per l'avvio di una macchina virtuale basica (VM) o istanza. Servizi opzionali come \textit{Block Storage} o \textit{Object Storage} richiedono nodi addizionali.\\
Si noti che l'esempio di architettura che verrà ora presentato è una configurazione minima e non è inteso a fini di installazioni di sistemi di produzione. \'E stato progettato per fornire una minima prova di concetto con lo scopo di apprendere nozioni di base su OpenStack.\\
Questo esempio differisce da un'architettura di produzione minimale per i seguenti due punti:
\begin{itemize}
	\item gli agenti di rete risiedono nel nodo \textit{controller} invece che in più nodi di rete dedicati
	\item la sovrapposizione del traffico di rete per le reti self-service attraversano la rete manageriale invece di quella dedicata.
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{Images/hwreqs.png}
	\caption{Architettura minimale del sistema}
\end{figure}
\newpage
Il nodo \textit{controller} esegue:
\begin{itemize}
	\item \textit{Identity service}, il servizio che fornisce le API per l'autenticazione degli utenti
	\item \textit{Image service}, un servizio che provvede alla memorizzazione delle immagini delle macchine virtuali e alla manutenzione di un catalogo delle immagini disponibili
	\item alcune porzioni del progetto che si occupano della computazione e alcune che si occupano del networking \footnote{Sistema di collegamento in rete di più elaboratori e utenti, comprendente le piattaforme, i sistemi operativi, i protocolli e le architetture di rete.}
\end{itemize}
Inoltre include alcuni servizi di supporto come un database SQL, una coda di messaggi e il protocollo NTP (\textit{Network Time Protocol}). Opzionalmente questo nodo può anche eseguire porzioni di \textit{Block Storage}, \textit{Object Storage}, \textit{Orchestration} e altri servizi di Telemetria.\\
Il nodo controller richiede un minimo di due interfacce di rete.
\newline
Il nodo computazionale (\textit{compute node}) esegue la porzione di hypervisor che esegue le istanze; di default questo nodo utilizza la macchina virtuale basata sul kernel ossia KVM (hypervisor). Inoltre questo nodo esegue dei servizi di rete che connettono le istanze a delle reti virtuali che forniscono servizi di firewalling.\\ 
\'E possibile utilizzare più di un compute node ma ognuno di essi richiede un minimo di due interfacce di rete.
\newline
Il nodo opzionale di Black Storage contiene i dischi che i servizi di Black Storage e di Shared File System forniscono per le istanze. Per semplicità il traffico tra i compute node e questo nodo utilizza la rete principale, gli ambienti di produzione dovrebbero implementare due reti di memorizzazione separate per incrementare performance e sicurezza. \'E possibile utilizzare uno o più nodi di questo tipo ma ognuno di essi richiede minimo un'interfaccia di rete.
\newline
Il nodo opzionale Object Storage contiene i dischi che il servizio di Object Storage utilizza per memorizzare account, contenitori ed oggetti. Per semplicità il traffico tra i compute node e questo nodo utilizza la rete principale, gli ambienti di produzione dovrebbero implementare due reti di memorizzazione separate per incrementare performance e sicurezza.\\
Questo servizio richiede almeno due nodi ed ogni nodo richiede minimo un interfaccia di rete.
\newpage
\section{Raccogliere dati}
Per raccogliere dati in maniera affidabile OpenStack utilizza principalmente due servizi, il primo è chiamato \textit{Telemetry} \cite{telemetry} lo scopo di questo servizio è quello di raccogliere dati sull'utilizzo fisico e virtuale delle risorse dei cloud in esecuzione. Questo servizio si occupa inoltre di mantenere i dati di modo che siano continuamente reperibili ed analizzabili.\\
Ad oggi il progetto Telemetry fornisce un insieme di funzionalità divise in diversi progetti più piccoli, ognuno dei quali è stato progettato per fornire un servizio caratteristico.\\
Tra i più rilevanti si ricordano:
\begin{itemize}
	\item \textit{Ceilometer}, un servizio per la raccolta dei dati che permette di normalizzare e trasformare tali dati in tutte le componenti di OpenStack.
	\item \textit{Gnocchi} un \textit{time-series database}\footnote{Sistema software ottimizzato per la gestione di \textit{time series data} ossia array di numeri indicizzati in base al tempo.} e servizio per l'indicizzazione delle risorse.
\end{itemize}
Il secondo servizio si chiama \textit{Monasca}, \cite{monasca} tale servizio si occupa di fornire una soluzione Monitoring-as-a-Service (\textit{MaaS}) che risulta essere \textit{multi-tenant}, questo significa per quanto l'istanza di tale servizio sia eseguita da un singolo server, questa è utilizzata da diverse organizzazioni.\\
MaaS è uno dei molti modelli per la consegna dei servizi di cloud computing; in particolare questo modello implica delle strutture per lo sviluppo di funzionalità di monitoraggio allo scopo di supportare altri servizi ed applicazioni all'interno del cloud.
\section{Live Migration}
In OpenStack la migrazione permette agli amministratori di spostare le istanze di macchine virtuali da un nodo ad un altro. Uno scenario tipico che può presentarsi è quello della manutenzione pianificata dell'host originario; la migrazione può essere inoltre utile per la redistribuzione del carico nel momento in cui molte istanze vengono eseguite su una stessa macchina fisica.\\
Esistono due tipi differenti di migrazione.
\begin{itemize}
	\item \textit{Non-live migration}: anche conosciuta come ``\textit{cold migration}'' o semplicemente migrazione; l'istanza viene spenta, poi spostata ad un altro \textit{hypervisor} e poi riavviata. L'istanza riconosce di essere stata riavviata e l'applicazione che stava eseguendo nell'istanza viene temporaneamente interrotta.
	\item \textit{Live migration}: Il termine \textit{live migration} si riferisce al processo di spostare una macchina virtuale in esecuzione o un'applicazione tra due differenti macchine fisiche senza disconnettere l'utente o l'applicazione; la memoria RAM, quella secondaria e la connettività di rete della macchina virtuale vengono trasferite dalla macchina ospite originale alla macchina destinataria.\\ 
	Questo tipo di migrazione può essere classificata ulteriormente a seconda di come vengono trattate le istanze di memorizzazione.
	\begin{itemize}
		\item \textit{Shared storage-based live migration}: l'istanza ha dei dischi effimeri localizzati in una memoria condivisa tra l'host originario e quello destinatario.
		\item \textit{Block live migration}: l'istanza non ha dei dischi effimeri localizzati in una memoria condivisa tra l'host di origine e quello destinatario; questo tipo di migrazione è incompatibile con i dispositivi di sola lettura.
		\item \textit{Volume-backed live migration}: le istanze utilizzano dei volumi piuttosto che dei dischi effimeri.
	\end{itemize}
	la \textit{block live migration} richiede la copia dei dischi dalla sorgente al destinatario di conseguenza richiede più tempo e ripone più carico sulla rete; al contrario le altre due tipologie non richiedono il processo di copia dei dischi.
\end{itemize}
\begin{figure}
	\hspace{-2.5cm}
	\includegraphics[width=18cm]{Images/livemigration.pdf}
	\caption{Diagramma di flusso della \textit{live migration} in OpenStack}
\end{figure}
\newpage
\section{Watcher}
Ai fini di questa tesi il focus è stato centralizzato per lo più sul servizio di ottimizzazione delle risorse implementato da OpenStack, tale servizio è chiamato \textit{Watcher} e risulta essere un servizio alquanto flessibile e scalabile. \textit{Watcher} fornisce un ciclo di ottimizzazione completo a partire dalla ricezione delle metriche, per passare poi all'elaborazione di eventi complessi, fino all'ottimizzazione ed all'applicazione di un piano di azione.\\
Tutto ciò crea una robusta struttura per la realizzazione di una vasta gamma di obbiettivi riguardanti l'ottimizzazione dei cloud, inclusi la riduzione dei costi operativi del centro dati, l'incremento delle performance di sistema attraverso una migrazione intelligente di macchine virtuali, la crescita dell'efficienza energetica e molto altro ancora.\\
Prima di andare ad analizzare nel dettaglio gli algoritmi di ottimizzazione proposti da OpenStack, viene proposta un analisi generale  sull'architettura \cite{architecture} di tale progetto, qui di seguito viene mostrato infatti un diagramma che mostra le principali componenti che verranno poi spiegate.
\begin{figure}[H]
	\hspace{-1cm}
	\centering
	\includegraphics[width=15cm]{Images/watcherarchitecture.png}
	\caption{Principali componenti di \textit{Watcher}}
\end{figure}
Prima di iniziare a spiegare l'utilità delle varie componenti è necessario dare alcune definizioni \cite{glossary}:
\begin{itemize}
	\item Una \textit{action} è ciò che permette al Watcher di trasformare lo stato corrente di un cluster dopo un \textit{audit}; un audit non è altro che una richiesta di ottimizzazione del cluster.
	\item Un \textit{action plan} specifica un flusso di \textit{action} che deve essere eseguito per soddisfare uno specifico obbiettivo.
	\item Una strategia è l'implementazione di un algoritmo in grado di trovare una soluzione per un dato obbiettivo; possono esserci diverse strategie che soddisfano il medesimo obbiettivo per questo motivo è possibile configurare quale strategia si vuole utilizzare per un particolare obbiettivo. Si noti che alcune strategie potrebbero fornire migliori risultati dal punto di vista dell'ottimizzazione ma potrebbero richiedere più tempo per trovare una soluzione ottimale. 
\end{itemize}
Il \textit{message bus} gestisce le comunicazioni asincrone interne tra le varie componenti del Watcher, il \textit{datasource} memorizza le metriche relative al cluster, la componente \textit{Watcher API} implementa le REST API fornite dal sistema Watcher per il mondo esterno.\\
\textit{Watcher Applier} è la componente che si occupa di eseguire l'action plan costruito dal \textit{watcher decision engine}; \textit{Watcher CLI} ossia l'interfaccia del watcher per la linea di comando oppure \textit{Watcher Dashboard} è ciò che può essere utilizzato per interagire con il sistema Watcher di modo da controllarlo o da sapere il suo stato corrente. \textit{Watcher Database} memorizza tutti gli oggetti con dominio Watcher che possono essere richiesti da Watcher API o Watcher CLI, in questo caso il dominio Watcher è ``ottimizzazione di alcune risorse fornite dal sistema di OpenStack''.\\
Una delle componenti con maggiore rilevanza è sicuramente il \textit{Watcher Decision Engine}, questo è responsabile per la computazione di un insieme di azioni per l'ottimizzazione, di modo da soddisfare un determinato obbiettivo di un Audit, prima legge i parametri dell'Audit per sapere qual'è l'obbiettivo da raggiungere dopodiché seleziona la strategia più appropriata tra quelle che soddisfano l'obbiettivo richiesto (a meno che non ne venga specificata una particolare) e la esegue.\\




OpenStack implementa due tipi di nodi principali (i nodi altro non sono che computer host fisici):
\begin{itemize}
	\item \textit{compute node}: questo nodo esegue gli hypervisor anche chiamati monitor di macchine virtuali, questi sono dei software, hardware o firmware che si occupano di creare ed eseguire le istanze delle macchine virtuali; 
	\item \textit{controller node}: tale nodo gestisce una porzione di computazione, della connessione di rete e diversi agenti di rete, questo è infatti il nodo in cui in molte configurazioni risiede il \textit{Watcher}.
\end{itemize}
Questo tipo di organizzazione rende la natura dell'architettura centralizzata, questo significa che esiste un nodo, il \textit{controller node}, che controlla e gestisce gli altri nodi ai fini dell'esecuzione e della buona riuscita delle diverse operazioni necessarie, tra cui le strategie di ottimizzazione che verranno descritte nella sezione successiva. 
\section{Strategie}
Per ogni strategia vengono in primis specificati i criteri utilizzati per determinare il carico di lavoro: 
\begin{center}
	\begin{tabular}{|p{5.3cm}|p{6.3cm}|}
		\hline
		\textbf{Strategia} & \textbf{Criterio}\\
		\hline
		Basic offline consolidation & Utilizzo della CPU\\
		\hline
		Outlet temperature based strategy & Temperatura esterna\\
		\hline
		Uniform airflow migration strategy & Flusso d'aria\\
		\hline
		VM Workload consolidation strategy & Utilizzo relativo di un nodo (rapporto tra utilizzo effettivo e capacità del nodo) basato sulle misurazioni di CPU, RAM e disco.\\
		\hline
		Workload balance migration strategy & Percentuale di utilizzo di CPU e RAM\\
		\hline 
	\end{tabular}
\end{center}
A questo punto vengono specificate una ad una tutte le strategie dopo una breve descrizione, in seguito per ognuna di esse si sottolineerà quale evento determina la migrazione e come viene scelto il destinatario.

\paragraph*{Basic offline consolidation} Il consolidamento delle macchine virtuali è essenziale per raggiungere l'ottimizzazione del carico energetico in ambienti che implementano i cloud come OpenStack; dato che le macchine virtuali vengono avviate e/o spostate nel corso del tempo, diventa necessario migrarle tra i vari server per abbassare i costi.\\
Si noti però che la migrazione delle macchine virtuali introduce dei costi aggiuntivi nei tempi di esecuzione e consuma dell'energia extra, di conseguenza una buona strategia di consolidamento deve pianificare attentamente la migrazione di modo da minimizzare il consumo di energia ed allo stesso tempo ottemperare le richieste dell'accordo sul livello del servizio.\\
Questo algoritmo non solo minimizza il numero totale di server utilizzati ma anche il numero di migrazioni.
\begin{itemize}
	\item Per scegliere il destinatario si scorrono tutti i nodi e la funzione \texttt{check\_migration} controlla se sono disponibili sufficienti risorse per la migrazione nel nodo preso in analisi.
	\item La migrazione è ritenuta necessaria quando il livello di utilizzo di uno o più nodi è troppo alto.
\end{itemize}

\paragraph*{Outlet temperature based strategy} La temperatura dell'aria in uscita è una nuova telemetria 
\begin{comment}
processo di comunicazione automatico grazie al quale vengono raccolte misurazioni ed altri dati in punti remoti o inaccessibili e che vengono poi ricevuti da un gruppo per il loro controllo 
\end{comment} 
termica che può essere utilizzata per misurare lo stato termico e del carico di lavoro di un host. Questa strategia prende decisioni con lo scopo di migrare il carico di lavoro su degli host che hanno delle buone condizioni termiche, quando la temperatura dell'aria in uscita dell'host sorgente raggiunge una soglia configurabile dall'utente.
\begin{itemize}
	\item Vengono messi nella lista \texttt{hosts\_target} i nodi la cui temperatura esterna è minore della soglia specificata, successivamente tali nodi vengono dati in input alla funzione \texttt{filter\_dest\_servers} che ritorna solo i nodi con sufficienti risorse disponibili. 
	\item La migrazione avviene se le liste \texttt{hosts\_need\_release} e \texttt{dest\_servers} sono non vuote.
\end{itemize}

\paragraph*{Uniform airflow migration strategy} Questa strategia di migrazione è basata sul flusso d'aria dei server fisici, genera delle soluzioni per spostare le macchine virtuali quando il flusso d'aria di un server raggiunge dei valori più alti della soglia specificata.
\begin{itemize}
	\item I possibili nodi di destinazione sono contenuti nella lista \texttt{nonoverload\_hosts} e questi sono quei nodi che hanno un flusso d'aria minore della soglia specificata; in seguito vengono dati in input alla funzione \texttt{filter\_destination\_hosts} che ritorna solo i nodi con sufficienti risorse disponibili.
	\item La migrazione avviene se le liste \texttt{source\_hosts} e \texttt{destination\_hosts} sono non vuote.
\end{itemize}

\paragraph*{VM workload consolidation strategy} Questa è una strategia di consolidamento del carico si focalizza sulle misurazioni di utilizzo della CPU e che tenta di minimizzare il numero di host che hanno troppo o troppo poco carico rispetto ai vincoli di capacità delle risorse. Questa strategia produce una soluzione che rende più efficiente l'utilizzo delle risorse del cluster, attraverso le seguenti quattro fasi: l'\textit{offload phase} che si occupa delle risorse troppo utilizzate,la \textit{consolidation phase} che si occupa delle risorse sotto utilizzate, la fase di \textit{solution optimization} che riduce il numero di migrazioni e la \textit{disabling phase} nella quale si disabilitano i compute node non utilizzati.
\begin{itemize}
	\item La scelta del destinatario dipende dalla situazione in cui si trova il \texttt{source\_node}:
	\begin{itemize}
		\item se questo è troppo utilizzato si scorrono tutti i nodi passandoli come input uno ad uno alla funzione \texttt{instance\_fits} che ritorna \texttt{true} se e solo se il nodo preso in analisi è in grado di ospitare la macchina virtuale che verrà migrata.
		\item se invece il \texttt{source\_node} è sotto utilizzato si prendono in considerazione quei nodi che hanno i più alti livelli di utilizzo della CPU e tramite la funzione \texttt{instance\_fits} si controlla quale di questi ha abbastanza spazio per ospitare il carico.
	\end{itemize}
	\item La migrazione avviene se vi sono effettivamente presenti dei nodi troppo o troppo poco utilizzati e se si riesce a trovare un \texttt{destination\_node}.
\end{itemize}

\paragraph*{Workload balance migration strategy} Questa strategia migra le macchine virtuali basandosi sul carico di lavoro delle stesse sugli host; decide di migrare il workload ogni qual volta la percentuale di utilizzo della RAM o della CPU di un host supera la soglia specificata. La macchina virtuale che deve essere spostata dovrebbe rendere il carico di lavoro di quell'host più vicino al carico di lavoro medio di tutti gli altri host.\\
\begin{itemize}
	\item Per quanto riguarda la scelta del destinatario vengono messi nella lista \texttt{target\_hosts} solo i nodi facenti parte della lista \texttt{nonoverload\_hosts}, ossia quei nodi che non sono sovraccarichi; in seguito la lista \texttt{target\_hosts} viene data in input alla funzione \texttt{filter\_destination\_hosts} che ritorna solo i nodi con sufficienti risorse disponibili.
	\item La migrazione avviene se le liste \texttt{target\_hosts} e \texttt{source\_nodes} sono non vuote.
\end{itemize}




