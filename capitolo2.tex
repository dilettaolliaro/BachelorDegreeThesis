\chapter{Simulazioni stocastiche}
%\addcontentsline{toc}{chapter}{Teoria delle simulazioni}
In questo capitolo verranno presentati i principali aspetti delle simulazioni di modo da poter capire in ogni suo aspetto il progetto realizzato che verrà successivamente presentato.\\
Una simulazione non è altro che un esperimento prodotto a computer ove l'ambiente reale viene rimpiazzato dall'esecuzione di un programma.\\
Il primo concetto necessario per la comprensione dell'argomento è la differenza tra tempo reale e tempo simulato; in una simulazione il flusso del tempo è controllato dal computer, in primo luogo sarà necessario simulare un certo parallelismo in quanto nei sistemi reali più azioni possono avvenire contemporaneamente, nel programma bisognerà serializzarle. Questo viene fatto tenendo conto del cosiddetto \textit{tempo simulato}, ossia il tempo in cui un evento dovrebbe avvenire nel sistema reale. Ogni azione  viene poi decomposta in eventi istantanei (i.e. l'arrivo di un utente nel sistema) e si assume che sia possibile che due eventi avvengano esattamente nello stesso momento.\\
Il tempo reale di una simulazione, come anticipato in precedenza, dipende invece dalle performance del proprio computer (i.e. velocità del processore, quantità di memoria) e dal livello di ottimizzazione del proprio programma di simulazione.\\
\newpage
\paragraph*{Sistemi, modelli e variabili} Prima di entrare nel vivo dell'argomento è necessario definire alcuni concetti di base.
\begin{itemize}
	\item Un \textit{sistema} è una collezione di componenti che interagiscono fra di loro in modo tale che il funzionamento soddisfi certe specifiche. Ad ogni istante di tempo un sistema è in una particolare condizione, detto stato; questo viene determinato dalle condizioni istantanee di tutti i componenti del sistema. Si noti che un sistema che evolve nel tempo è descritto dalla storia degli stati.
	\item Un \textit{modello} di un sistema è una sua rappresentazione matematica, fisica o analitica, infatti il modello di un sistema reale ha  un certo livello di astrazione; è importante ricordare che il modello in sé non rappresenta la storia del sistema infatti allo stato del modello è sempre associato uno stato del sistema reale.
	\item Un modello presenta diversi tipi di \textit{variabili}:
	\begin{itemize}
		\item variabili \textit{esogene}, ossia quelle che dipendono dall'ambiente e che se risultano essere controllabili prendono il nome di \textit{parametri}
		\item variabili \textit{endogene}, quelle variabili che  dipendono dal modello stesso, ossia i risultati in output
		\item variabili \textit{di stato}, ossia quelle che descrivono lo stato del sistema in un certo istante e variano nel tempo interagendo con le variabili esogene ed endogene; si ricordi inoltre che le interazioni tra le varie variabili sono definite dalle caratteristiche operative. 
	\end{itemize}
\end{itemize}
Simulare un modello significa produrre la storia degli stati del modello che viene interpretata come la storia degli stati del sistema; il modello simulato è detto \textit{modello di simulazione}, mentre la simulazione al calcolatore viene chiamata \textit{simulazione digitale}.\\
L'adeguatezza di un modello di un sistema è giudicata alla luce degli scopi dello studio.
\newpage
\section{Tipi di simulazione}
Ci sono diversi tipi di simulazione, solitamente vengono utilizzate le seguenti classificazioni.
\paragraph*{Deterministiche/Stocastiche} Una simulazione deterministica non ha componenti randomiche, questo tipo di simulazione viene utilizzata quando si vuole verificare un sistema di cui l'ambiente è interamente conosciuto, magari per testare la solidità di una certa implementazione.\\
In molti casi però questo tipo di simulazione non è sufficiente, l'ambiente di un sistema è modellabile in maniera migliore aggiungendo delle componenti casuali, il che rende di conseguenza randomico anche l'output della simulazione.
\paragraph*{Terminanti/Non terminanti} Una simulazione terminante finisce quando si verifica una particolare condizione; se per esempio si volesse valutare il tempo di esecuzione di una sequenza di operazioni in un ambiente ben definito, è possibile eseguire la sequenza nel simulatore e tenere conto del tempo simulato. Tipicamente si utilizzano simulazioni terminanti quando si è interessati al tempo di vita di un certo sistema o quando gli input dati al sistema dipendono dal tempo.
\paragraph*{Asintoticamente stazionarie/Non stazionarie} La condizione di stazionarietà si può verificare solo nel caso di simulazioni stocastiche non terminanti, in quanto la stazionarietà è una proprietà dei modelli stocastici che vengono simulati.\\
Molto spesso, lo stato di una simulazione dipende dalle condizioni iniziali ed è difficile trovare delle condizioni iniziali favorevoli; ad esempio se si volesse simulare un server di informazione probabilmente si partirebbe con un buffer vuoto, ma questa ipotesi risulta troppo ottimistica nella realtà dei fatti in quanto in un sistema reale che viene eseguito da una certa quantità di tempo saranno contenute delle strutture dati che non saranno vuote.\\
La stazionarietà è una soluzione a questo tipo di problema, infatti una simulazione stazionaria, che ha quindi un unico regime stazionario, è tale da non farci ricavare alcuna informazione utile dal suo stato iniziale in quanto la sua distribuzione degli stati diventa indipendente dalle condizioni iniziali.\\
Nella pratica, purtroppo, una simulazione risulta raramente essere esattamente stazionaria ma può invece essere asintoticamente stazionaria; questo significa che dopo un certo tempo di simulazione, la simulazione arriverà al suo stato di stazionarietà.\\
Più precisamente, una simulazione con input indipendenti dal tempo può sempre essere pensata come una catena di Markov, si ricordi che una catena di Markov è un generico processo stocastico tale che al fine di simulare il futuro dopo un certo tempo $t$, l'unica informazione di cui si ha bisogno è lo stato del sistema al tempo $t$ (\textit{proprietà dell'assenza di memoria}); la teoria delle catene di Markov dice che la simulazione potrà o convergere ad un comportamento stazionario oppure divergere. Nel caso in cui si vogliano misurare le performance di un sistema in analisi è più probabile che si sia interessati al suoi comportamento stazionario.\\
Nella pratica ci sono diverse ragioni perché non si verifichi una stazionarietà asintotica.
\begin{itemize}
	\item Modelli instabili: nei sistemi a coda dove il tasso di arrivo è più grande della capacità di servizio, l'occupazione del buffer cresce in maniera illimitata di conseguenza più lungo è il tempo d'esecuzione della simulazione più grande sarà la lunghezza media della coda.
	\item \textit{Freezing simulation}: il sistema simulato non converge al regime stazionario ma invece si congela diventando sempre più lento. Questo tipo di comportamento è tipicamente dovuto all'avvenimento di qualche evento raro che altera drasticamente il funzionamento del sistema. La probabilità di osservare un evento raro è tanto più alta quanto è più lungo il tempo di simulazione. Se, per caso, la simulazione dovesse avere dei punti di rigenerazione \footnote{Punti in cui viene raggiunto uno stato pulito, ad esempio quando il sistema si svuota} allora la simulazione si congela se l'intervallo di tempo tra due punti di rigenerazione ha media infinita.
	\item Modelli con input dipendenti dal tempo di simulazione, in questo tipo di modelli è proprio la dipendenza dell'input dal tempo di simulazione ad impedire il raggiungimento di un regime stazionario; si consideri per esempio il traffico internet che cresce di mese in mese e che risulta più intenso in alcuni momenti della giornata.
\end{itemize}
Si tenga in particolare considerazione il fatto che lo stato iniziale non influisce sul raggiungimento del regime stazionario ma influisce sul tempo richiesto per raggiungere tale regime; inoltre è importante ricordare che in stazionarietà non diventa costante lo stato del modello ma si stabilizza invece la probabilità di fare una certa osservazione.\\
Nella maggior parte dei casi quando si esegue una simulazione non-terminante è necessario assicurarsi che questa raggiunga un regime di stazionarietà; altrimenti, l'output della simulazione dipenderà dalle condizioni iniziali e dalla lunghezza della simulazione stessa, si consideri anche che spesso è impossibile decidere quali siano delle condizioni iniziali realistiche.
\subsection{Tecniche di simulazione}
In questa sezione verranno presentate le tre principali classi di simulatori esistenti.\\
\paragraph*{Simulazione per eventi (\textit{Discrete Event Simulation - DES})}
In questo tipo di simulazione, che verrà spiegato in dettaglio nella prossima sezione, si modellano un insieme di entità il cui stato evolve nel tempo; queste interagiscono per competere per delle risorse o per sincronizzarsi su determinati eventi. Il punto cruciale di questo tipo di simulazione è il mantenimento della lista di eventi che devono essere processati.
\paragraph*{Dinamica dei sistemi (\textit{System dynamics - SD})}
Rispetto alla tipologia precedentemente presentata l'approccio risulta top-down infatti viene modellato l'intero sistema piuttosto che le sue entità; per modellare il legame tra le variabili di stato vengono utilizzate le equazioni differenziali. Questa tecnica è utilizzata quando il sistema consiste di un gran numero di entità uguali o molto simili ed è dunque più appropriato considerarne la loro numerosità piuttosto che la loro identità.
\paragraph*{Simulazione per agenti (\textit{Agent Based Simulation - ABS})}
Questa tecnica consiste nel simulare il processo decisionale di ogni singolo agente che compone il sistema; gli agenti possono essere diversi tra loro ed avere diverse conoscenze sullo stato del sistema, l'obbiettivo finale solitamente è quello di osservare dei comportamenti emergenti a livello di sistema come risultato delle interazioni tra gli agenti. Questo risulta essere un paradigma emergente ed ancora poco utilizzati a livello industriale.
\newpage
\section{Introduzione alla simulazione discreta}
La maggior parte dei sistemi di comunicazione vengono simulati utilizzando la simulazione discreta per eventi.\\
Per la comprensione di questo modello è necessario avere ben presente i concetti di entità, ossia gli elementi che interagiscono del modello di una simulazione e di evento inteso come il cambiamento di stato di una o più entità.\\
Il punto focale di questa metodologia è quello di tenere conto di una variabile globale che avrà il nome di \texttt{currentTime} e di uno scheduler di eventi.\\
Gli eventi sono oggetti che rappresentano diverse transizioni, ogni evento ha un suo tempo associato che prenderà il nome di \texttt{timestamp}; uno scheduler di eventi non è altro che una lista di eventi ordinata in maniera crescente in base al \texttt{timestamp}.\\
Ciò che accade è che il programma di simulazione prende il primo evento da questa lista, sposta il \texttt{currentTime} al \texttt{timestamp} dell'evento ed esegue l'evento. L'esecuzione dell'evento potrebbe schedulare nuovi eventi con un \texttt{timestamp} maggiore del \texttt{currentTime} e potrebbe anche cambiare o cancellare eventi che erano precedentemente nella lista degli eventi. Si noti che la variabile globale \texttt{currentTime} non deve essere assolutamente modificata da un evento nonostante questa salti dal \texttt{timestamp} di un evento a quello dell'evento successivo (da cui il nome simulazione discreta per eventi\footnote{Si parla infatti di simulazione discreta quando gli eventi che cambiano lo stato di un sistema formano un insieme numerabile (discreto)}). Si noti che oltre a simulare la logica del sistema gli eventi devono anche tenere aggiornati i contatori che verranno poi utilizzati per le statistiche.\\
\newline
Nella pratica un simulatore orientato agli eventi viene così realizzato; si mantiene un calendario degli eventi, chiamato \textit{Future Event List} - FEL, dal quale viene estratto quello che avviene prima. Un sottoprogramma gestisce l'evento modificando lo stato di tutte le entità influenzate dall'evento ed eventualmente modificando il valore delle variabili endogene (output), lo stesso sottoprogramma aggiorna il calendario degli eventi. Vengono proposti qui di seguito due schemi che presentano in maniera semplice ed intuitiva il funzionamento di un simulatore per eventi:
\begin{figure}[H]
	\centering
	\begin{minipage}[c]{.45\textwidth}
		\centering
%		\centering\setlength{\captionmargin}{1pt}
		\includegraphics[width=1.2\textwidth]{Images/ciclofondamentale.png}
		\caption{Il ciclo fondamentale}
	\end{minipage}
	\hspace{10mm}
	\begin{minipage}[c]{.45\textwidth}
		\centering
	%	\centering\setlength{\captionmargin}{1pt}
		\includegraphics[width=0.5\textwidth]{Images/processaevento.png}
		\caption{Processa evento}
	\end{minipage}
\end{figure}
Per quanto riguarda la gestione della FEL vengono richieste alcune operazione di base, quali: l'inserimento di un evento, il prelievo del prossimo evento da processare e la cancellazione di un evento arbitrario, ossia di un evento che non sia necessariamente quello con il \texttt{timestamp} più piccolo.
\newline
Alla luce di queste nuove conoscenze è necessario ricordare la definizione di tempo reale, il tempo del sistema da simulare, tempo simulato ossia la rappresentazione del tempo reale nel modello di simulazione che avanza a salti in corrispondenza di eventi critici, e tempo di esecuzione ossia il tempo consumato dall'elaboratore per condurre la simulazione.\\
L'avanzamento del tempo simulato può procedere per intervalli fissi, dunque il tempo avanza di $\Delta t$ ad ogni passo e devono essere simulati tutti gli eventi che accadano in detto intervallo di tempo (tali eventi saranno considerati simultanei)\footnote{In questo caso nascono una serie di problematiche in quanto sarà necessario scegliere come trattare i multipli eventi e come scegliere $\Delta t$} oppure può procedere per eventi pertanto il tempo avanza di una quantità pari al tempo necessario a far accadere il più vicino tra gli eventi possibili schedulati nel calendario degli eventi.
\section{Analisi dei risultati di una simulazione}
Per quanto riguarda le simulazioni non terminanti è necessario distinguere due fasi, una fase transiente (fase iniziale o di warm up) ed una fase stazionaria che generalmente viene raggiunta, come spiegato in precedenza, per $t\rightarrow \infty$ dove $t$ rappresenta il tempo simulato.\\
Le statistiche raccolte durante la fase transiente dipendono dallo stato iniziale pertanto non sono di alcun interesse al fine di un analisi statistica dei risultati ottenuti.\\
Il problema principale che si pone nel caso di simulazioni non terminanti è proprio quando farle terminare, in quanto non è possibile affidarsi, nella pratica, alla condizione di stazionarietà $t\rightarrow \infty$ ed è necessario riuscire a capire quando si è arrivati in regime stazionario poiché è lì che verranno raccolte le statistiche di interesse.\\ 
\newline
Il metodo che si utilizza per ovviare a questo problema è il metodo grafico di Welch che funziona come spiegato qui di seguito.\\
Si supponga di voler stimare la media di una sequenza di osservazioni intese come variabili casuali $Y_1, Y_2, Y_3...$ in regime stazionario, si vuole in pratica stimare:
$$\upsilon = \lim\limits_{i\rightarrow \infty} \valormedio[Y_i]$$
In pratica si avrà una sequenza finita di osservazioni $Y_i$ con $i\in [1,m]$ e si cercherà un $l$ che consenta di definire:
$$\bar{Y}_l=\dfrac{\sum_{i=l}^{m} Y_i}{m-l}$$
dove $\bar{Y}_l$ è la variabile casuale media delle osservazioni da $l+1$ a $m$; la speranza è quella di avere $\upsilon \approxeq \valormedio[\bar{Y}_l]$.\\
Si noti che è importante scegliere accuratamente $l$ perché se dovesse essere troppo piccolo $\bar{Y}_l$ sarebbe distorto dalle osservazioni iniziali che non sono stazionarie, mentre se dovesse essere scelto troppo grande (quindi vicino a $m$) il numero di variabili casuali utilizzate per definire $\bar{Y}$ sarebbe troppo piccolo e l'accuratezza della stima di $\upsilon$ sarebbe troppo bassa.\\
\newline
Si procede come segue: si eseguono $n$ esecuzioni pilota (con un $n$ possibilmente maggiore o uguale a 5), ciascuna esecuzione risulterà in $m$ osservazioni, $Y_{ji}$ sarà la variabile casuale associata all'osservazione $i$-esima dell'esecuzione $j$ con $1 \leq j \leq m$. Si definisca ora la sequenza costituita dalle medie, il che ridurrà la varianza del processo:
$$\bar{Y}_i=\dfrac{\sum_{j=1}^{n}Y_ji}{n}$$
A questo punto si crea una finestra di larghezza $w\leq \lfloor m/4\rfloor$ e si costruisce una sequenza di $m-w$ variabili casuali ciascuna delle quali sarà definita come la media delle $w$ $\bar{Y}$ precedenti, quella corrente e le $w$ $\bar{Y}$ successive:
$$\bar{Y}^{'}_{i}=\dfrac{\sum_{s=i-w}^{i+w}\bar{Y}_s}{2w+1}$$
si noti che per le variabili casuali da 1 a $w$ la media si calcola come:
$$\bar{Y}^{'}_{i}=\dfrac{\sum_{s=1}^{2i-1}\bar{Y}_s}{2i-1}$$
Infine si pongono in un grafico le $\bar{Y}^{'}_{i}$ osservazioni per $i=1,...,m-w$ e si sceglie il valore di $l$ oltre il quale la serie di $\bar{Y}_i$ sembra essersi stabilizzata.\\
\newline
Riprendendo il discorso sopra citato sappiamo che le simulazioni non terminanti non hanno un naturale punto di arresto, in qualche modo però è necessario decidere quando interrompere la simulazione e con essa la raccolta delle statistiche. Intuitivamente sarebbe ideale interrompere la simulazione quando la qualità delle stime ottenute risulta ragionevolmente buona.\\ In pratica l'idea basilare per valutare la qualità delle stime di una simulazione consiste nell'eseguire delle esecuzioni indipendenti e poi confrontarne le stime. La regola empirica imporrebbe di far durare una simulazione il tempo sufficiente per far si che un numero di esecuzioni ripetute diano stime che si presentano in maniera approssimativa come una variabile casuale Gaussiana.\\
\'E necessario però prestare attenzione a non prolungare eccessivamente una simulazione in quanto potrebbe provocare dei problemi non da poco come una certa inconsistenza nei risultati ottenuti a causa del consumo di tempo e risorse, dell'instabilità numerica dei floating point e della periodicità dei generatori di numeri casuali.