\chapter{Progetto} \label{cap:progetto}
L'obbiettivo principale di questa tesi è arrivare ad una valutazione generica dei due principali tipi di strategie di migrazione che possono essere utilizzate in un cloud:
\begin{itemize}
	\item le strategie centralizzate: esiste un nodo che detiene tutte le informazioni su gli altri nodi quindi sa quando è il caso di avviare una migrazione e sa anche quale deve essere il nodo sorgente e quale quello destinatario, OpenStack utilizza metodi di questo tipo;
	\item le strategie distribuite: in questo caso sono i nodi stessi a richiedere le informazioni agli altri nodi nel momento in cui si rendono conto di essere o troppo carichi (\textit{overloaded}) o troppo scarichi (\textit{underloaded}).
\end{itemize}
Intuitivamente è chiaro che applicare un'organizzazione centralizzata è rischioso in quanto dovesse succedere qualcosa al nodo che detiene tutte le informazioni l'intero sistema sarebbe a rischio.\\
In linea generale ci si aspetterà che su cloud con pochi nodi siano più efficienti strategie centralizzate, che diventeranno invece meno efficienti su cloud con molti nodi in quanto sarà più dispendioso in termini computazionali trovare il miglior mittente ed il miglior destinatario.\\
Allo stesso tempo le strategie distribuite potrebbero non rivelarsi sempre la soluzione migliore, in quanto generalmente tali strategie si preoccupano di trovare una soluzione nell'immediato ma che potrebbe non rivelarsi la più ottimizzata nel lungo termine.\\
Ora che gli scopi analitici sono stati espressi, è possibile presentare quello che è stato il progetto svolto per poi valutarne i risultati e verificare quali delle supposizioni precedenti risulta convalidata o meno.
\section{Descrizione}
Il progetto implementato è un simulatore orientato agli eventi che presenta una simulazione terminante, per svolgerlo si è scelto di utilizzare il linguaggio di programmazione Java per sfruttare al meglio il suo paradigma ad oggetti, le sue peculiari caratteristiche sulla ereditarietà delle classi e soprattutto per la sua tendenza a facilitare la programmazione ad eventi.\\
Il simulatore è stato progettato di modo da risultare adattabile a diversi tipi di test tramite una buona parametrizzazione dei parametri, è infatti possibile scegliere se avere un sistema centralizzato o distribuito, la quantità di nodi nel cloud, il tasso di arrivo e di servizio, la politica di scheduling e di migrazione e la quantità di macchine virtuali che arrivano nel sistema; tramite un piccola modifica si potrebbe rendere tale simulazione non terminante non ponendo un limite al numero di macchine che arrivano nel sistema.\\
Nel progetto vi sono anche alcune caratteristiche che rimangono costanti:
\begin{itemize}
	\item il tasso di arrivo segue la distribuzione di una variabile casuale di Poisson di parametro $\lambda$,
	\item il tasso di servizio segue la distribuzione di una variabile casuale esponenziale di parametro $\mu$,
	\item le macchine virtuali sono considerate tutte con la stessa dimensioni e
	\item le condizioni secondo cui un nodo viene considerato overloaded o underloaded sono sempre le stesse; un nodo viene ritenuto troppo carico se ha nella sua coda di esecuzione otto o più macchine virtuali, viene invece ritenuto scarico se ne ha due o meno.
\end{itemize}
Si noti inoltre che tali nodi vengono considerati ad otto core, di conseguenza le macchine presenti in un nodo ricevono tutte servizio contemporaneamente; questo risulta di particolare importanza quando si scelgono i valori di $\lambda$ e $\mu$, è necessario infatti tenere presente che la condizione di stabilità è data da:
$$\lambda<64\mu$$
Superato tale limite il sistema non viene più considerato stabile nonostante, come si vedrà dopo, si è ritenuto di particolare interesse vedere come si comportano le diverse configurazioni quando il sistema è sottoposto ad un carico largamente superiore al limite imposto dalla condizione di stabilità.\\
Inoltre, si ricordi che se una macchina virtuale arriva nel sistema e trova tutti i nodi completamente occupati questa viene persa.\\
\'E importante infine specificare che le strategie di migrazione utilizzabili sono tre, due distribuite ed una centralizzata, e sono:
\begin{itemize}
	\item \textit{Receiverbased} policy: quando un nodo completa il servizio ad una determinata macchina virtuale controlla se risulta ancora sovraccarico, in tal caso cerca casualmente un nodo scarico e appena ne trova uno gli manda le macchine in eccesso.
	\item \textit{Senderbased} policy: quando una macchina virtuale viene affidata ad un nodo questo controlla se lui stesso è \textit{underloaded}, in questo caso cerca un altro nodo casualmente se lo trova \textit{overloaded} si prende macchine di modo da bilanciare il carico.
	\item \textit{Centralized} policy: il nodo \textit{controller} monitora la situazione dei nodi ogni volta che una macchina virtuale entra o esce dal sistema; se un nodo è troppo carico gli toglie macchine virtuali per darle ad altri nodi meno carichi stando attendo a non sovraccaricarli, in caso contrario se un nodo è troppo scarico cerca dei nodi \textit{overloaded} di modo da togliergli macchine e darle a questo.
\end{itemize}
\subsection{Configurazione scelta}
Le configurazioni scelte per effettuare i vari test è la seguente:
\begin{figure}[H]
	\centering
	\includegraphics[width=8cm]{Images/distributedcloud.png}
	\caption{Configurazione di un cloud distribuito}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=11cm]{Images/centralizedcloud.png}
	\caption{Configurazione di un cloud centralizzato}
\end{figure}
La configurazione presenta otto nodi più uno controller nel caso in cui si scelga un'organizzazione centralizzata e il numero di default di macchine virtuali che arrivano nel sistema è impostato a 32000.\\
Il valore di $\mu$ è settato a 1 e non cambia nel corso dei test effettuati mentre il valore di $\lambda$ assume i valori multipli di 8 compresi nell'intervallo $[1,128]$.\\
La politica di dispatching utilizzata di default è quella random, di modo da evitare di porre il sistema in situazioni ideali, mentre vengono utilizzate tutte e tre le politiche di migrazione nel corso dei test.\\
\section{Struttura}
Il progetto è suddiviso in quattro pacchetti: \texttt{cloudsimulator}, \texttt{migrator}, \texttt{dispatcher} e \texttt{random}.
\paragraph*{cloudsimulator} Questo è il package principale, contiene la classe principale ossia \texttt{CloudSimulator.java} che si preoccupa dell'avvio, dell'esecuzione e della terminazione della simulazione stessa. Il cuore della simulazione è contenuto nella classe \texttt{Simulation.java} che si preoccupa di inizializzare e gestire la \textit{Future Event List}, definita nella classe \texttt{FutureEventList.java} ed organizzata come una coda di priorità ordinata in base al campo \texttt{timestamp} contenuto negli oggetti di tipo \texttt{Event}.\\
\texttt{Event.java} è una classe astratta implementata poi da due sottoclassi che descrivono i due eventi principali della simulazione, queste classi si chiamano: \texttt{OnVMArrivalEvent.java} che gestisce gli arrivi e \texttt{OnVMExpirationEvent.java} che gestisce le partenze.\\
Infine ci sono altre tre classi contenute in questo pacchetto che servono per descrivere le entità principali del sistema: \texttt{Node.java}, \texttt{ControllerNode.java} e \texttt{VirtualMachine.java} 
\paragraph*{dispatcher} In questo pacchetto è contenuta la classe \texttt{Dispatcher.java} che implementa tre diverse politiche di dispatching che potrebbero essere utilizzate per test futuri; le tre politiche sono quella casuale che viene utilizzata nei test i cui risultati verranno presentati in seguito, la politica \textit{ShortestQueueFirst} che manda la macchina virtuale in arrivo nel nodo che ne ha di meno e la politica \textit{LowestLoadFirst} che manda la macchina virtuale in arrivo nel nodo ``meno carico'', per carico si intenda il tempo di \textit{ExpirationTime}, calcolato tramite variabile casuale esponenziale di parametro $\mu$.
\paragraph*{migrator} In questo pacchetto è posta la classe \texttt{migrator.java} che si occupa di applicare le tre diverse strategie di migrazione.
\paragraph*{random} Qui sono contenuti dei \textit{wrapper}, ossia delle classi che permettono di utilizzare in maniera meno complicata l'implementazione del generatore di numeri casuali Marsenne Twister messa a disposizione.
\section{Risultati}
Ai fini di questa tesi risulta interessante comprendere con quale strategia di migrazione il carico nel cloud risulta bilanciato, per fare questo è stata considerata la deviazione standard media delle code dei nodi al variare del tasso di arrivo; questa metrica ci da un indice di dispersione ossia una stima della variabilità di una popolazione di dati, più questa è alta meno il cloud sarà bilanciato e viceversa.\\
Altri indici considerati sono:
\begin{itemize}
	\item il throughput calcolato come segue:
	$$\dfrac{n\_vms-droppedVMs}{n\_vms}$$
	ossia con la configurazione impostata per i nostri test:
	$$\dfrac{32000-droppedVMs}{32000}$$
	si consideri $n\_vms$ il numero di macchine virtuali che vengono inviate al sistema e $droppedVMs$ il numero di macchine virtuali che vengono perse perché al loro arrivo trovano tutti i nodi completamente pieni; dunque il throughput è dato dal rapporto tra il numero di macchine virtuali il cui dispatching avviene con successo e il numero di macchine virtuali totale che arriva nel sistema.
	\item e il numero di migrazioni che avvengono nel corso di una simulazione al variare dei valori di $\lambda$; più tale numero è alto più la strategia di migrazione resiste bene al carico ricevuto.
\end{itemize}
\newpage
	\subsection{Deviazione Standard}
	\begin{center}
		\begin{tabular}{cccc}
			
			\boldmath$\lambda$ & \textbf{Centralized} & \textbf{Receiverbased} & \textbf{Senderbased}\\
			\toprule
			1.0 & 0.4635 & 0.4632 & 0.4637 \\

			8.0 & 1.2060 & 1.2053 & 1.2026 \\

			16.0 & 1.7906 & 1.8025 & 1.7965 \\
			
			24.0 & 2.1733 & 2.1931 & 2.2082 \\
			
			32.0 & 2.3621 & 2.4137 & 2.4775 \\
			
			40.0 & 2.4133 & 2.5639 & 2.5866 \\
			
			48.0 & 2.4410 & 2.6470 & 2.6071 \\
			
			56.0 & 2.5746 & 2.6751 & 2.6304 \\
			
			64.0 & 2.7179 & 2.7398 & 2.6936 \\
			
			72.0 & 2.8357 & 2.8024 & 2.7729 \\
			
			80.0 & 2.9509 & 2.8733 & 2.8597 \\
			
			88.0 & 3.0012 & 2.9024 & 2.9022 \\
			
			96.0 & 3.0402 & 2.9210 & 2.9322 \\
			
			104.0 & 3.0593 & 2.9307 & 2.9456 \\
			
			112.0 & 3.0781 & 2.9370 & 2.9614 \\
			
			120.0 & 3.0642 & 2.9186 & 2.9440 \\
			
			128.0 & 3.0783 & 2.9236 & 2.9497 \\
			\bottomrule
		\end{tabular}
	\end{center}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/StdDevOnLambdaC.png}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/StdDevOnLambdaRB.png}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/StdDevOnLambdaSB.png}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=1.08\textwidth]{Images/StrategiesDevStd.png}
		\caption{Deviazione Standard delle tre strategie a confronto}
	\end{figure}
	\subsection{Throughput}
	\begin{center}
		\begin{tabular}{cccc}
			\boldmath$\lambda$ & \textbf{Centralized} & \textbf{Receiverbased} & \textbf{Senderbased }\\
			\toprule
			1.0 & 1.0 & 1.0 & 1.0 \\

			8.0 & 1.0 & 1.0 & 1.0 \\
			
			16.0 & 1.0 & 1.0 & 1.0 \\
			
			24.0 & 1.0 & 1.0 & 1.0 \\
			
			32.0 & 1.0 & 1.0 & 1.0 \\
			
			40.0 & 1.0 & 1.0 & 1.0 \\
			
			48.0 & 0.999 & 0.999 & 0.999 \\
			
			56.0 & 0.968 & 0.968 & 0.968 \\
			
			64.0 & 0.908 & 0.908 & 0.908 \\
			
			72.0 & 0.835 & 0.835 & 0.835 \\
			
			80.0 & 0.768 & 0.768 & 0.768 \\
			
			88.0 & 0.707 & 0.707 & 0.707 \\
			
			96.0 & 0.647 & 0.647 & 0.647 \\
			
			104.0 & 0.606 & 0.606 & 0.606 \\
			
			112.0 & 0.563 & 0.563 & 0.563 \\
			
			120.0 & 0.524 & 0.524 & 0.524 \\
			
			128.0 & 0.493 & 0.493 & 0.493 \\
			\bottomrule
		\end{tabular}
	\end{center}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/ThroughputOnLambdaC.png}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/ThroughputOnLambdaRB.png}
	\end{figure}
	\begin{figure}[H]
		\centering\setlength{\captionmargin}{0pt}
		\includegraphics[width=0.95\textwidth]{Images/ThroughputOnLambdaSB.png}
	\end{figure}
	\subsection{Numero di migrazioni}	
	\begin{center}
		\begin{tabular}{cccc}
			\boldmath$\lambda$ & \textbf{Centralized} & \textbf{Receiverbased} & \textbf{Senderbased}\\
			\toprule
			1.0 & 0 & 0 & 0 \\
			
			8.0 & 5 & 1 & 1 \\
			
			16.0 & 76 & 60 & 44 \\
			
			24.0 & 571 & 479 & 500 \\
			
			32.0 & 2072 & 1162 & 2154 \\
			
			40.0 & 4952 & 1265 & 5850 \\
			
			48.0 & 12241 & 557 & 12543 \\
			
			56.0 & 19014 & 180 & 12892 \\
			
			64.0 & 20108 & 39 & 10469 \\
			
			72.0 & 18424 & 10 & 7754 \\
			
			80.0 & 16070 & 2 & 5456 \\
			
			88.0 & 13780 & 2 & 4334 \\
			
			96.0 & 11703 & 1 & 3252 \\
			
			104.0 & 10264 & 2 & 2633 \\
			
			112.0 & 8918 & 0 & 2099 \\
			
			120.0 & 7703 & 1 & 1755 \\
			
			128.0 & 6787 & 1 & 1401 \\
			\bottomrule
		\end{tabular}
	\end{center}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/NumMigrationOnLambdaC.png}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/NumMigrationOnLambdaRB.png}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.95\textwidth]{Images/NumMigrationOnLambdaSB.png}
	\end{figure}
	\begin{figure}[H]
		\includegraphics[width=1.08\textwidth]{Images/StrategieNumMigration.png}
		\caption{Numero di migrazioni delle tre strategie a confronto}
	\end{figure}
