\chapter{Concetti teorici}
%\addcontentsline{toc}{chapter}{Concetti teorici}
Nel seguente capitolo verranno analizzati e descritti i fondamenti teorici utilizzati come base di questa tesi, verranno presentati elementi essenziali della teoria delle code, di statistica \cite{stat,stat2} e di teoria delle simulazioni.
\section{Richiami di statistica}
Nella realizzazione del progetto si è implementato un simulatore stocastico, si è dunque fatto uso di distribuzioni esponenziali e processi di Poisson. \\
La distribuzione di Poisson è strettamente legata al concetto di ``\textit{eventi rari}'': tali eventi occorrono in tempi casuali, la probabilità che ci sia un nuovo evento durante un piccolo intervallo di tempo è proporzionale alla lunghezza dell'intervallo stesso. Il numero di eventi rari che avvengono in un periodo di tempo fissato hanno distribuzione di Poisson.\\
Una variabile casuale di Poisson può assumere un qualunque valore intero non negativo, in quanto il possibile numero di eventi risulta non limitato.\\ 
La distribuzione di Poisson ha un solo parametro, ossia $\lambda>0$ che rappresenta il numero medio degli eventi rari considerati, e la sua funzione di probabilità è:
$$P_{\lambda}(n)=\dfrac{\lambda^{n}}{n!} e^{-\lambda}\ \forall n\in\mathbb{N}$$
Questa distribuzione viene spesso utilizzata per modellare eventi come: l'arrivo di job, errori nel software, blackout di rete, etc.\\
Si noti che in una sequenza di eventi rari \footnote{Questi eventi rari vengono generati tramite processi di Poisson, concetto che verrà spiegato meglio in seguito.}, il cui numero segue una distribuzione di Poisson, il tempo che intercorre tra un evento e l'altro viene modellato attraverso una distribuzione esponenziale la cui funzione di densità di probabilità (definita sui numeri reali positivi) risulta essere pari alla funzione esponenziale:
$$f(x)=\lambda e^{-\lambda x}\ per\ x>0$$
Un'interessante proprietà di questa distribuzione continua è quella dell'assenza di memoria: se il tempo $t$ è già stato raggiunto, la probabilità di raggiungere un tempo $t+\Delta$ non dipende da $t$.\\
Per comprendere meglio tale concetto si consideri il seguente esempio: si supponga che una variabile esponenziale $T$ rappresenti un tempo di attesa; la proprietà dell'assenza di memoria significa che il fatto di aver atteso per $t$ minuti viene ``dimenticato'' e che tale fatto non influisce sul tempo di attesa futuro. Nonostante l'evento $T>t$, quando il tempo di attesa eccede $t$, il tempo d'attesa rimanente è comunque una distribuzione esponenziale con lo stesso parametro.\\
Questo concetto può essere scritto matematicamente come:
$$\pr\{T>t+x|T>t\}=\pr\{T>x\}\ t,x>0$$
Come si è già descritto l'esponenziale viene spesso utilizzata per modellare il tempo, in quanto risulta essere un ottimo modello per descrivere i tempi di interarrivo; si consideri una sequenza di eventi rari, ove il numero di occorrenze durante il tempo $t$ ha una distribuzione di Poisson con parametro proporzionale a $t$, questo tipo di processo è ciò che chiamiamo un processo di Poisson.\\
Per spiegare questo concetto è necessario prima introdurre la nozione di processo stocastico: un processo stocastico è una variabile casuale che dipende dal tempo, di conseguenza è una funzione a due argomenti, $X(t, \omega)$, dove:
\begin{itemize}
	\item $t\in \mathcal{T}$ è il tempo, con $\mathcal{T}$ insieme di possibili tempi, solitamente: $[0,\infty), $ $(-\infty,\infty),{0,1,2,...}\ o\ {...,-2,-1,0,1,2,...}$;
	\item $\omega\in\Omega$, ossia il risultato di un esperimento, con $\Omega$ che rappresenta l'intero spazio di campionamento.
\end{itemize}
I valori di $X(t,\omega)$ sono chiamati ``\textit{stati}''.\\
Un processo stocastico può essere continuo se $\mathcal{T}$ è un intervallo connesso ed illimitato oppure il processo è definito discreto se l'insieme $\mathcal{T}$ è composto fa punti isolati e distinti.\\
Qui di seguito verranno introdotti due processi stocastici, fondamentali per la comprensione di ciò che verrà successivamente spiegato riguardo la teoria delle code (Sezione \ref{sec:queuetheory}).
\paragraph*{Processi di Markov} Un processo stocastico è detto di Markov se per ogni $t_1,<...<t_n<t$ ed ogni insieme $A_1,...,A_n$ si ha che:
\begin{equation} 
\pr{X(t)\in A|X(t_1)\in A_1,...X(t_n)\in A_n}=\pr{X(t)\in A|X(t_n)\in A_n} 
\label{eq:markov}
\end{equation} 
L'equazione \ref{eq:markov} significa che la distribuzione condizionale di $X(t)$ è la stessa sotto due differenti condizioni:
\begin{enumerate}
	\item vengono fornite osservazioni del processo $X$ in diversi momenti del passato;
	\item vengono fornite le ultime osservazioni di $X$.
\end{enumerate}
Dunque se un processo è Markoviano, il suo comportamento futuro è lo stesso indipendentemente dalle condizioni (1) e (2); in altre parole, conoscendo il presente non è possibile ottenere alcuna informazione dal passato che possa essere utilizzata per predire il futuro.\\
Di conseguenza, per il futuro comportamento di un processo di Markov, è importante solamente lo stato presente e non il come il processo sia arrivato a tale stato.\\
In seguito verrà fatto riferimento alle cosiddette ``\textit{catene di Markov}'', una catena di Markov non è altro che un processo Markoviano con spazio degli stati discreto, dunque un processo stocastico che assume valori in uno spazio discreto e che gode della proprietà di Markov sopra spiegata. 
\paragraph*{Processo di Poisson} \label{par:processoPoisson} Un processo di Poisson è un processo stocastico a tempo continuo che descrive il manifestarsi di eventi che siano tra loro indipendenti e che accadono continuamente nel tempo.\\
Oltre ad essere un esempio di catena di Markov a tempo continuo, poiché ne rispetta le proprietà, risulta interessante in quanto il processo viene definito tramite una collezione di variabili aleatorie $X(t)\ con\ t>0$ interpretate come il numero di eventi occorsi dal tempo $0$ al tempo $t$; inoltre si noti che il numero di eventi intercorsi tra il tempo $a$ e $b$, è dato come $t_b-t_a$ ed ha una distribuzione di Poisson.
\newpage
\section{Generatori di numeri (pseudo)casuali}
Il calcolatore è una macchina deterministica, salvo particolari tipi di hard-
ware siamo in grado di generare solo numeri che risultano essere pseudocasuali e non completamente randomici.
\'E possibile limitare il problema alla generazione di numeri casuali nell’intervallo $(0,1)$ con distribuzione uniforme \footnote{In statistica una distribuzione uniforme continua o discreta è una distribuzione di probabilità che risulta uniforme su un insieme dato, questo significa che tale distribuzione attribuisce la stessa probabilità a tutti i punti appartenenti ad un dato intervallo $[a,b]$ contenuto nell'insieme dato. Si noti che una qualunque distribuzione di probabilità univariata (ossia sui numeri reali) è legata alla distribuzione uniforme.}, da questa vengono poi generate le altre distribuzioni.\\
I generatori di numeri casuali devono avere le seguenti proprietà:
\begin{itemize}
	\item i numeri generati devono appartenere all’intervallo (0,1), devono esse-
	re distribuiti uniformemente e non vi deve essere correlazione tra le
	estrazioni
	\item devono essere veloci e non devono occupare troppa memoria
	\item le estrazioni devono essere riproducibili
	\item devono essere a disposizione diversi stream di numeri casuali
	\item devono essere portabili.
\end{itemize}
La generazione di numeri pseudocasuali è un argomento molto delicato in
quanto molti generatori usati in software/librerie noti non sono in realtà
adatti per essere usati nelle simulazioni.\\
Qui di seguito verrà data una definizione formale \cite{rng} di generatore di numeri (pseudo)casuali, dopodiché verrà presentato il generatore utilizzato nel progetto svolto (argomento del capitolo \ref{cap:progetto}), ossia il generatore \textit{Marsenne Twister} \cite{mt}.\\
\paragraph*{Definizione formale} Matematicamente un generatore di numeri (pseudo)casuali può essere definito come una quintupla $(S, \mu, f, U, g)$, dove $S$ è un insieme finito di stati, $\mu$ è una distribuzione di probabilità su $S$ usata per selezionare lo stato iniziale $s_0$, chiamato ``\textit{seed}'' (seme), $f: S \rightarrow S$ è la funzione di transizione, $U=[0,1]$ è l'insieme di output ed, infine, $g: S \rightarrow U$ è la funzione di output.\\
\newpage
Lo stato evolve secondo la ricorrenza 
$$s_i=f(s_{i-1})\ per\ i\geq1$$
 e l'output del passo $i$ risulta:
$$u_i = g(s_i)\in U$$
questi $u_i$ sono i cosiddetti numeri casuali prodotti dal generatore.\\

Dato che l'insieme $S$ è finito il generatore prima o poi tornerà ad uno stato già visitato in precedenza, ossia:
$$s_{l+j}=s_l \textrm{ per qualche } l\geq0 \textrm{ e } j>0$$
Dunque 
$$s_{i+j}=s_i \textrm{ e } u_{i+j}=u_i\  \forall\ i\geq l $$
Il più piccolo valore di $j>0$ per cui questo accade è chiamato ``\textit{lunghezza del periodo}'', indicata con la lettera greca $\rho$, tale valore chiaramente non può essere più grande della cardinalità di $S$; in particolare se $b$ bit sono utilizzati per rappresentare uno stato allora avremo $\rho \leq 2^{b}$.\\
I generatori di numeri (pseudo)casuali migliori sono costruiti in modo che la lunghezza del loro periodo si avvicini il più possibile a tale limite superiore.

\paragraph*{Marsenne Twister, Random Number Generator} Il Marsenne Twister è un generatore di numeri casuali, ad oggi è il generatore \textit{general-purpose} più largamente utilizzato; fu sviluppato nel 1997 da Makoto Matsumoto e Takuji Nishimura.\\
L'algoritmo alla base di questo generatore genera un ottimo insieme di numeri pseudocasuali, questo perché nel crearlo si è cercato di porre rimedio ai difetti presentati dai diversi generatori di numeri pseudocasuali esistenti all'epoca.\\
Il nome di questo generatore deriva dal fatto che la lunghezza del suo periodo è scelta di modo che risulti essere un numero primo di Marsenne \footnote{In matematica un numero primo di Marsenne è un numero primo che soddisfi la seguente proprietà: $$M_p=2^p-1$$ con $p$ intero positivo primo.}.\\
\newpage
Questo generatore offre diversi vantaggi, tra i quali:
\begin{itemize}
	\item un periodo enorme equivalente a $2^{19937}-1$, fatto dimostrato formalmente dai creatori dell'algoritmo;
	\item ha passato diversi test statistici di casualità;
	\item risulta essere molto più efficiente di altri algoritmi che magari non lo equiparano minimamente nella qualità;
	\item è estremamente portabile, è infatti incluso in diversi linguaggi di programmazione e librerie.
\end{itemize}
\section{Teoria delle code} \label{sec:queuetheory} 
I sistemi a coda sono delle strutture composte da uno o più server progettati per eseguire alcuni \textit{task} \footnote{Attività di esecuzione di un programma in modo sequenziale, ovvero un compito che il processore dell'elaboratore deve portare a termine su richiesta dell'utente.} o per processare alcuni \textit{job} \footnote{Unità di lavoro che uno scheduler invia al sistema operativo.} e da una coda di job che aspettano di essere processati.\\
Dunque, riassumendo: un job arriva nel sistema a coda, aspetta un server disponibile, viene processato e poi lascia il sistema.
\begin{figure}[H]
	\centering
	\includegraphics[width=12cm]{Images/SistemiACoda.png}
	\caption{Rappresentazione grafica di un generico sistema a coda}
\end{figure}
La teoria delle code \cite{qt} non è altro che lo studio matematico di questo tipo di sistemi, in questa sezione andremo ad introdurre la notazione di Kendall per le code, diversi collegamenti con le nozioni statistiche viste in precedenza ed alcuni principali modelli di code per capire in seguito il modello utilizzato per il progetto argomento di questa tesi.
\newline
La notazione di Kendall è il sistema standard che viene utilizzato per descrivere e classificare i vari tipi di code, il concetto principale è che possiamo descrivere una coda utilizzando sei fattori (gli ultimi tre spesso sottintesi) indicati come segue: $$A/S/c/K/N/D$$
dove:
\begin{itemize}
	\item ``$A$'' identifica la distribuzione che descrive il processo di arrivo, ai fini di questa tesi è sufficiente introdurre il codice $M$ che sta ad indicare che gli arrivi sono distribuiti secondo un processo Markoviano come ad esempio un processo di Poisson, per cui i tempi di interarrivo possono essere descritti con una distribuzione esponenziale.
	\item ``$S$'' identifica la distribuzione che descrive i tempi di attesa, come sopra introduciamo solamente la notazione ``$M$'' che sta sempre per processo Markoviano ed indica che il tempo di servizio segue una distribuzione esponenziale.
	\item ``$c$'' è il numero di server presenti nel sistema.
	\item ``$K$'' è il numero di spazi nel sistema, ossia la sua capacità, si intendano dunque il massimo numero di clienti \footnote{Nel sistema che si sta descrivendo clienti e job vengano intesi come lo stesso concetto} ammessi nel sistema compresi quelli in elaborazione; quando tale numero viene raggiunto, ulteriori richieste vengono rifiutate. Se nella notazione della coda viene omesso questo fattore lo si assume illimitato. 
	\item ``$N$'' è la cardinalità della popolazione dalla quale provengono i clienti; una piccola popolazione può influire molto sul rate di arrivo effettivo in quanto man mano che i job arrivano in coda ce ne sono sempre meno che possono arrivare nel sistema. Se tale parametro viene omesso la popolazione è assunta illimitata.
	\item ``$D$'' indica la disciplina \cite{qd} di servizio applicata alla coda di attesa, ossia l'ordine in cui si è scelto di servire i job in coda, le più frequentemente utilizzate sono:
	\begin{itemize}
		\item FCFS (\textit{First-Come-First-Served}), i clienti sono serviti nell'ordine in cui sono arrivati e la loro elaborazione viene completata prima che un altro cliente riceva il servizio (disciplina \textit{non-preemptive}\footnote{In italiano, non prelazione, è l'operazione per cui se ad un processo viene assegnata una risorsa di elaborazione non gli viene mai tolta fino a che tale processo non ha completato l'esecuzione.}); tale disciplina è quella assunta di default nel caso non dovesse essere specificato il fattore ``$D$'' nella notazione.
		\item LCFS (\textit{Last-Come-First-Served}), i clienti vengono serviti nell'ordine inverso rispetto a quello in cui sono arrivati, questa politica è spesso utilizzata quando i job sono disposti in una pila.
		\item SIRO (\textit{Service-In-Random-Order}), i clienti sono serviti secondo un ordine randomico che non tiene affatto conto dell'ordine di arrivo, questa politica è spesso utilizzata per scopi teorici.
		\item PNPN (\textit{Priority Service}), i clienti sono serviti secondo un ordine di priorità scelto.
	\end{itemize}
\end{itemize}

Verranno ora analizzati qui di seguito, da un punto di vista più formale, quelli che risultano essere i componenti principali di un sistema a coda, ossia: l'arrivo di un job, il suo instradamento in un server, la sua elaborazione e la sua partenza.\\
Tipicamente i job arrivano in un sistema a coda con dei tempi casuali, un processo ``\textit{contatore}''\footnote{Verrà così chiamato un processo che si occupa di tenere conto di una certa quantità.} tiene traccia del numero di arrivi avvenuti fino al tempo $t$; in un sistema a coda stazionario (sistema in cui le caratteristiche della distribuzione non cambiano nel tempo), gli arrivi avvengono con un tasso di arrivo pari a:
$$\lambda=\dfrac{\valormedio A(t)}{t}\ \forall t>0$$
che sarebbe il numero atteso di arrivi per unità di tempo, di conseguenza il tempo atteso fra gli arrivi è dato da $$\dfrac{1}{\lambda}$$
I job che arrivano vengono processati secondo l'ordine scelto dalla disciplina di servizio applicata al sistema; quando un job arriva, questo può trovare il sistema in differenti stati:
\begin{itemize}
	\item se un server è disponibile si prenderà certamente in carico il job in attesa
	\item se diversi server sono disponibili, il job può essere mandato in uno di questi server in maniera randomica o secondo le leggi di una particolare politica di \textit{scheduling} applicata al sistema. Ad esempio potrebbe essere mandato nel server più veloce o in quello che presenta un carico minore
	\item infine se tutti i server sono occupati ad elaborare altri job, quello in questione resterà in coda fino a che un qualche job in elaborazione non sarà completato.
\end{itemize}
Si noti che un sistema può presentare diversi altri vincoli. Per esempio una coda potrebbe avere un \textit{buffer} \footnote{Dispositivo di memoria usato per la conservazione provvisoria di dati destinati ad essere rielaborati.} che limiti il numero di job in attesa, questo significherebbe che il sistema in questione ha una capacità limitata, se tale capacità dovesse essere riempita un nuovo job non potrebbe entrare nel sistema prima che uno di quelli in elaborazione termini. Inoltre i job potrebbero lasciare la coda d'attesa prematuramente, ad esempio dopo tempi di attesa eccessivamente lunghi.
\newline
Una volta che un server si libera, inizierà a processare il job successivo; nella pratica i tempi di servizio sono randomici in quanto dipendono dalla quantità di lavoro richiesto per ogni task.\\
Il tasso di servizio è definito come il numero medio di job processati da un server che lavora in maniera continua nell'arco di un'unità di tempo, dunque se definiamo come $\mu$ il tempo medio di servizio, si avrà un tasso di servizio equivalente a:
$$\dfrac{1}{\mu}$$
Dopo che il servizio viene completato il job lascia il sistema.\\
Riassumiamo nella tabella sottostante i parametri che descrivono le performance di un sistema a coda:\\
\begin{center}
	\begin{tabular}{|c|l|}
		\hline
		\underline{Notation}&\underline{Parametri di un sistema a coda}\\
		$\lambda$ & tasso di arrivo\\
		$\mu$ & tasso di servizio\\
		$1/\lambda$ & tempo di interarrivo medio\\
		$1/\mu$ & tempo medio di servizio\\
		$\rho=\dfrac{\lambda}{\mu}$ & utilizzo o rapporto arrivo/servizio\\
		\hline
	\end{tabular}
\end{center}
L'utilizzo $\rho$ è un importante parametro in quanto mostra se un sistema presenta prestazioni accettabili con il tasso di arrivo corrente o addirittura con un tasso di arrivo più alto; inoltre mostra anche quanto un sistema è sovraccarico o se invece è sottoposto ad un carico insufficiente.
\subsection{Code M/M/1 e M/M/c}
Verranno presentati in questa sezione due modelli molto utilizzati di sistemi a coda, prima però è necessario introdurre due nozioni ad essi legati di rilevante importanza: la legge di Little e i processi di nascita e morte.\\
\paragraph*{Legge di Little} La legge di Little crea una semplice relazione fra il numero atteso di job, il response time atteso ed il tasso di arrivo; essa implica che in un sistema a coda in \textit{stato stazionario}\footnote{In inglese, \textit{steady state}, un sistema si definisce in stato stazionario se le variabili che definiscono il comportamento del sistema non cambiano nel tempo.} il numero medio di elementi in coda equivale al tasso medio a cui arrivano tali elementi moltiplicato per il tempo medio che un elemento passa nel sistema. Pertanto, ponendo:
\begin{itemize}
	\item $L$ = numero medio di elementi in coda
	\item $W$ = tempo medio di attesa per un elemento nel sistema
	\item $\lambda$ = numero medio di elementi in arrivo per unità di tempo
\end{itemize}
Avremo che la legge di Little sarà:
$$L=\lambda W$$
Nonostante questa sembri un'intuizione piuttosto semplice, si noti che tale relazione non viene influenzata né dalla distribuzione del processo di arrivo o del processo di servizio né tanto meno dall'ordine in cui si è scelto di servire i clienti; da questa legge è possibile trarre una conclusione tanto semplice quanto importante ossia che un elemento ha un tempo medio di permanenza nel sistema equivalente alla somma del tempo medio passato in coda d'attesa dall'elemento stesso e dal tempo medio che tale elemento necessita per ricevere il servizio richiesto.
\newpage
\paragraph*{Processo di nascita e morte} 
Il processo di nascita e morte è un caso speciale di processo stocastico Markoviano a tempo continuo dove gli stati di transizione sono solo di due tipi:
\begin{itemize}
	\item ``nascite'' evento che incrementa di uno la variabile di stato
	\item ``morti'' evento che decrementa la variabile di stato di uno
\end{itemize}
Tale processo prende il suo nome da un suo comune campo applicativo ossia il controllo della popolazione dove effettivamente le transizioni sono di nascita e di morte.\\
Dunque quando avviene una nascita il processo va dallo stato $n$ allo stato $n+1$, al contrario quando si verifica una morte si passa dallo stato $n$ allo stato $n-1$; tale processo viene descritto da un tasso di nascita $\{\lambda_{i}\}_{i=0...\infty}$ e tasso di morte $\{\mu_{i}\}_{i=1...\infty}$. Graficamente:
\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{Images/DeathAndBirthProcess.png}
	\caption{Diagramma di stato di un processo di nascita e morte} 
	\label{DBprocess}	
\end{figure}

\paragraph*{Coda M/M/1} 
In teoria delle code è definito ``Coda M/M/1'' un sistema a coda che ha un singolo server ove gli arrivi sono determinati tramite un processo di Poisson e i tempi di servizi dei job seguono una distribuzione esponenziale.\\
Una coda M/M/1 è un processo stocastico definito sui numeri naturali, in cui il valore ad un tempo fissato rappresenta il numero di utenti nel sistema inclusi quelli correntemente in elaborazione.\\
Viene qui di seguito brevemente definito il modello in maniera più specifica.
\begin{itemize}
	\item Gli arrivi avvengono secondo un processo di Poisson di parametro $\lambda$ e spostano il processo dallo stato $i$ allo stato $i+1$.
	\item I servizi seguono, come già anticipato, una distribuzione esponenziale di parametro $\mu$, dove $\dfrac{1}{\mu}$ rappresenta il tempo di servizio medio.
	\item Un server unico serve i clienti uno alla volta a partire dall'inizio della coda, utilizzando la disciplina \textit{First-Come-First-Served}. Quando l'elaborazione viene completata il cliente lascia la coda ed il numero di clienti nel sistema diminuisce di uno.
	\item Il buffer risulta di dimensione infinita, dunque non c'è un limite al numero di clienti che può contenere.
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=8cm]{Images/mm1queue.png}
	\caption{Rappresentazione grafica di una coda M/M/1}
\end{figure}
Questo modello può essere descritto come una catena di Markov a tempo continuo, si noti che può essere la stessa catena di Markov che descrive il processo di nascita e morte; il diagramma di stato per questa catena è uguale a quello rappresentato nella figura \ref{DBprocess}.\\
Tale modello viene definito stabile se e solo se $\lambda<\mu$, questo perché in linea generale se gli arrivi sono più di quanti il sistema possa servirne, la coda crescerà in maniera indefinita di conseguenza il sistema non avrà una distribuzione stazionaria né potrà considerarsi equilibrato.\\
Sono diverse le metriche di performance che si possono considerare per questo sistema, tra le più importanti ricordiamo $\rho=\dfrac{\lambda}{\mu}$ che rappresenta il livello di utilizzo del buffer, tale valore deve essere $\rho <1$ perché il sistema rispetti la condizione stabilità; detto in altre parole $\rho$ rappresenta la proporzione di tempo per cui il server risulta occupato.\\
Tramite le precedenti osservazioni è possibile calcolare anche il numero di utenti nel sistema e il periodo di attività del server ossia il periodo di tempo misurato a partire da quando un utente arriva in un sistema vuoto fino a quando questo non lascia il sistema. \'E possibile anche calcolare il tempo medio di soggiorno di un utente nel sistema, si noti che questo non dipende dalla disciplina di scheduling utilizzata e che di conseguenza tale valore può essere calcolato utilizzando la legge di Little come:
$$\dfrac{1}{\mu - \lambda}$$
di conseguenza il tempo medio speso in attesa è dato da:
$$\dfrac{1}{\mu-\lambda}-\dfrac{1}{\mu}= \dfrac{\rho}{\mu-\lambda}$$
\newpage
\paragraph*{Coda M/M/c}
La coda M/M/c è un modello di coda multi-server, grazie alla notazione di Kendall possiamo intuire che questo modello descrive un sistema a coda dove gli arrivi ad una singola coda sono governati da un processo di Poisson, tali arrivi vengono poi serviti da $c$ server i cui tempi di servizio sono distribuiti esponenzialmente. \'E facile notare che questo modello risulta essere una generalizzazione del modello precedentemente presentato.\\
Qui di seguito è proposta una descrizione più specifica del sistema in analisi:
\begin{itemize}
	\item Gli arrivi avvengono secondo un processo di Poisson di parametro $\lambda$ e spostano il processo dallo stato $i$ allo stato $i+1$.
	\item I servizi seguono, come già anticipato, una distribuzione esponenziale di parametro $\mu$, dove $\dfrac{1}{\mu}$ rappresenta il tempo di servizio medio. In questo caso se sono arrivati nel sistema meno di $c$ job alcuni server saranno vuoti mentre se ne sono arrivati di più, i job saranno accodati in un buffer.
	\item Un server unico serve i clienti uno alla volta a partire dall'inizio della coda, utilizzando la disciplina \textit{First-Come-First-Served}. Quando l'elaborazione viene completata il cliente lascia la coda ed il numero di clienti nel sistema diminuisce di uno.
	\item Il buffer risulta di dimensione infinita, dunque non c'è un limite al numero di clienti che può contenere.
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=8cm]{Images/mmcqueue.png}
	\caption{Rappresentazione grafica di una coda M/M/c}
\end{figure}
\newpage
Questo modello può essere descritto come una catena di Markov a tempo continuo, si noti che può essere la stessa catena di Markov che descrive il processo di nascita e morte; il diagramma di stato per questa catena è leggermente diverso da quello nella figura \ref{DBprocess} e viene rappresentato come segue:
\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{Images/DBprocessmmc.png}
	\caption{Processo di nascita e morte di una coda M/M/c}
\end{figure}
Tale modello viene definito stabile se e solo se $\rho<1$, dove $$\rho=\dfrac{\lambda}{c\mu}$$
In questo caso è possibile affermare che $\rho$ rappresenta la proporzione di tempo per cui ogni server risulta occupato, assumendo che nel caso in cui i job trovino uno più server inutilizzati ne scelgano uno randomicamente.\\
Per quanto riguarda le metriche di performance considerabili si avranno le stesse metriche del modello presentato in precedenza ma con alcune piccole differenze:
\begin{itemize}
	\item Se l'intensità del traffico $\rho$ risulta maggiore di 1 la coda crescerà in maniera indefinita, nel caso contrario il sistema avrà una distribuzione stazionaria e sarà perciò possibile calcolare il numero di utenti nel sistema.
	\item Il periodo di occupazione del server in questo caso può riferirsi a due differenti periodi di tempo:
	\begin{itemize}
		\item periodo d'occupazione completa: il periodo di tempo che intercorre tra l'arrivo di un cliente che trova $c-1$ utenti nel sistema e la partenza di un utente che lascia il sistema con $c-1$ clienti al suo interno.
		\item periodo d'occupazione parziale: il periodo di tempo che intercorre tra l'arrivo di un cliente che trova il sistema vuoto e la partenza di un utente che lo lascia vuoto nuovamente.
	\end{itemize}
	\item Infine è possibile calcolare il tempo di risposta, comprensivo del tempo che un cliente passa in coda ed in elaborazione; il tempo medio di risposta è indipendente dalla disciplina di servizio applicata al sistema e si calcola:
	$$\dfrac{C(c,\lambda/\mu)}{c\mu-\lambda}+\dfrac{1}{\mu}$$
	ove $C(c,\lambda/\mu)$ è la probabilità che un utente in arrivo sia obbligato ad aggiungersi alla coda, in altre parole la probabilità che nel momento in cui arriva un utente tutti i server siano occupati.
\end{itemize}